import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public email: string = '';
  public password: string = '';
  constructor(private route: Router, private auth: AuthService) {}

  ngOnInit(): void {}
  navigate(page: string): void {
    this.route.navigate([page]);
  }
  login(): any {
    this.auth
      .login({ email: this.email, password: this.password })
      .subscribe((data: any) => {
        console.log(data);
        if (data.data) {
          this.auth.user = data.data;
          this.route.navigate(['dashboard']);
        }else{
          alert(data.message)
        }
      });
  }
}
