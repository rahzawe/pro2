import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public email: string = '';
  public name: string = '';
  public password: string = '';
  constructor(private route: Router, private auth: AuthService) {}

  ngOnInit(): void {}
  navigate(page: string): void {
    this.route.navigate([page]);
  }
  register(): any {
    this.auth
      .register({ email: this.email, name: this.name, password: this.password })
      .subscribe((data) => console.log(data));
  }
}
