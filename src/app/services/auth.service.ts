import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, Observer, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const URL_PREFIX = 'http://localhost:3000';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public user: any;
  constructor(private http: HttpClient) {}
  register(user: any) {
    return this.http.post(`${URL_PREFIX}/register`, user).pipe(
      catchError((error: HttpErrorResponse) => {
        console.log('Error while creating user', error);
        return throwError('Error while creating a user');
      })
    );
  }
  login(user: any) {
    return this.http.post(`${URL_PREFIX}/login`, user).pipe(
      catchError((error: HttpErrorResponse) => {
        console.log('Error while creating user', error);
        return throwError('Error while creating a user');
      })
    );
  }
}
